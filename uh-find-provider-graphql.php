<?php

/*
Plugin Name: Find a Provider GraphQL Interface
Description: An extension of the WPGraphQL plugin to provide support for the CFAR API
Author: UComm Web Team
Version: 1.1.4
Text Domain: uhfp-graphql
*/

use UHFPGraphql\Connections\ConnectionRegister;
use UHFPGraphql\Fields\FieldRegister;
use UHFPGraphql\Types\TypeRegister;
use UHFPGraphql\Mutations\MutationRegister;

if (!defined('WPINC')) {
  die;
}

define('UHFP_GRAPHQL_DIR', plugin_dir_path(__FILE__));
define('UHFP_GRAPHQL_URL', plugins_url('/', __FILE__));
define('FIND_PROVIDER_API_URL', 'https://publicdirectoryapi.uchc.edu/api');

// select the right composer autoload.php file depending on environment.
if (file_exists(dirname(ABSPATH) . '/vendor/autoload.php')) {
  require_once(dirname(ABSPATH) . '/vendor/autoload.php');
} elseif (file_exists(ABSPATH . 'vendor/autoload.php')) {
  require_once(ABSPATH . 'vendor/autoload.php');
} else {
  require_once('vendor/autoload.php');
}

require_once(ABSPATH . 'wp-admin/includes/plugin.php');
require_once('lib/AdminNotices.php');

$wp_graphql_active = is_plugin_active('wp-graphql/wp-graphql.php');
$uhfp_graphql_active = is_plugin_active('uh-find-provider-graphql/uh-find-provider-graphql.php');

// TODO - prevent this plugin from activating if wpgraphql is not already active
if (!$wp_graphql_active && $uhfp_graphql_active) {
  add_action('admin_notices', ['UHFPGraphql\AdminNotices', 'activationError']);
} else {

  require 'lib/HTTPRequest.php';
  require 'lib/NodeConverter.php';
  
  /**
   * Require custom fields
   */
  
  require 'lib/fields/FieldInterface.php';
  require 'lib/fields/CustomField.php';
  require 'lib/fields/ClinicField.php';
  require 'lib/fields/FinancialDisclosureField.php';
  require 'lib/fields/ProfessionalProfileField.php';
  require 'lib/fields/ProviderField.php';
  require 'lib/fields/FieldRegister.php';
  require 'lib/fields/SecureEmailTokenField.php';
  
  /**
   * 
   * Require custom types
   * 
   */
  require 'lib/types/CustomTypeInterface.php';
  require 'lib/types/CustomType.php';
  // require specific types
  require 'lib/types/Clinic.php';
  require 'lib/types/clinic/ClinicCategory.php';
  require 'lib/types/provider/Education.php';
  require 'lib/types/clinic/Location.php';
  require 'lib/types/Provider.php';
  require 'lib/types/provider/BoardSpecialty.php';
  require 'lib/types/provider/FinancialDisclosure.php';
  require 'lib/types/provider/HonorAndAwards.php';
  require 'lib/types/provider/HospitalAffilliation.php';
  require 'lib/types/provider/Language.php';
  require 'lib/types/provider/ProfessionalProfile.php';
  require 'lib/types/provider/ProviderCategory.php';
  require 'lib/types/provider/Subspecialty.php';
  require 'lib/types/provider/Training.php';
  require 'lib/types/provider/Website.php';
  require 'lib/types/Specialty.php';
  require 'lib/types/SecureEmailToken.php';
  // register required types
  require 'lib/types/TypeRegister.php';
  
  /**
   * 
   * Require resolvers
   * 
   */
  require 'lib/resolvers/ResolverInterface.php';
  require 'lib/resolvers/CustomResolver.php';
  require 'lib/resolvers/ClinicResolver.php';
  require 'lib/resolvers/provider/ConnectionResolver.php';
  require 'lib/resolvers/provider/FinancialDisclosureResolver.php';
  require 'lib/resolvers/provider/ProfessionalProfileResolver.php';
  require 'lib/resolvers/ProviderResolver.php';
  require 'lib/resolvers/SpecialtyResolver.php';
  require 'lib/resolvers/SecureTokenResolver.php';
  
  /**
   * Require connections
   */
  require 'lib/connections/ConnectionInterface.php';
  require 'lib/connections/CustomConnection.php';
  require 'lib/connections/ClinicToRoot.php';
  require 'lib/connections/ProviderToRoot.php';
  require 'lib/connections/provider/BoardSpecialtiesToProvider.php';
  require 'lib/connections/provider/ProfessionalProfileToProvider.php';
  require 'lib/connections/provider/ClinicsToProvider.php';
  require 'lib/connections/provider/SpecialtyToProvider.php';
  require 'lib/connections/provider/SubspecialtyToProvider.php';
  require 'lib/connections/provider/TrainingToProvider.php';
  require 'lib/connections/provider/EducationToProvider.php';
  require 'lib/connections/provider/HonorAndAwardsToProvider.php';
  require 'lib/connections/provider/HospitalAffilliationToProvider.php';
  require 'lib/connections/provider/WebsiteToProvider.php';
  require 'lib/connections/SpecialtyToRoot.php';
  require 'lib/connections/ConnectionRegister.php';

  // register custom mutations here if needed
  
  // create custom fields
  new FieldRegister();
  
  // create custom types
  new TypeRegister();
  
  // create custom connections
  new ConnectionRegister();

  // create new custom mutations
  // new MutationRegister();

}