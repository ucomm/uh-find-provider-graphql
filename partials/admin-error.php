<div class="notice notice-error">
  <p><?php _e('Oops! Looks like you haven\'t activated the WPGraphql plugin yet.', 'uhfp-graphql'); ?></p>
</div>