# Changelog
## 1.1.4
- Added support for provider NPI numbers
- Updated SDK and dependencies

## 1.1.3
- Removed dev checks to resolve production errors.

## 1.1.2
- Update to resolvers to handle dev and test environments better.

## 1.1.1
- Added a field for providers to handle financial disclosure information
- Updated dependencies
- Other improvements

## 1.1.0
- `wp_remote_get` is deprecated in this project due to health server constraints.

## 1.0.6
- Update for Find a Provider SDK

## 1.0.5
- Improved caching strategy for fetching all providers
- Increased timeout of all providers response

## 1.0.4
- Updated wp-graphql dependency
- Updated docker-compose web image

## 1.0.3
- Removed `require`s for mutations since they could have potentially caused problems.

## 1.0.2
- Removed secure email mutation from server implementation. We don't want patient data passing through the health aurora server system.

## 1.0.1
- Fixed timeout on request for all providers

## 1.0.0
- Initial release