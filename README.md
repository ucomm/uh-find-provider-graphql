# UH Find a Provider GraphQL Plugin

This (uh-find-provider-graphql) is an extension of the [wp-graphql](https://www.wpgraphql.com/) plugin. Its purpose is to provide a more developer friendly interface for [the CFAR API endpoints](https://test-publicdirectoryapi.uchc.edu/Help). We have an SDK that helps us make requests to those endpoints, and cache the responses. However the JSON responses are still very large and need to be filtered regardless.

## Usage

### Getting Started

```
$ composer install
$ docker-compose up
```

### Activation
The wp-graphql plugin must be active for this plugin to do anything. So turn that one on first, and then this one.

## GraphQL
Locally the graphql endpoint will be at [http://localhost/wordpress/graphql](http://localhost/wordpress/graphql). If it's working correctly, you will see a graphql error message in the browser. That's ok and expected. GraphQL only responds to queries or mutations. Sending nothing in the request to the endpoint does nothing.

wp-graphql should be active on health.dev and is available at [https://health.dev.uconn.edu/graphql](https://health.dev.uconn.edu/graphql).

### Making requests
There are several ways to interact with graphql. The easiest is to use a graphql playground like:

- [GraphiQL](https://github.com/graphql/graphiql)
- [Graphql Playground](https://github.com/prisma-labs/graphql-playground)

### Debugging

If there's no response from wp-graphql or this plugin, it can help to flush the permalinks and/or change the permalink structure.

Sometimes it can be hard to debug graphql queries. There are many [good resources on debugging in the wp-graphql docs](https://docs.wpgraphql.com/guides/debugging/).

You can also make a plain HTTP request to the query/mutation you're trying to perform using (e.g.) Postman. To try this, create a query param key/value in the form of `?query={graphql query}`. You can literally write it as a string that looks exactly like the query you're trying to perform. For instance this should work fine...

```
http://localhost/wordpress/graphql?query={ clinics {nodes { locationName } } }
```

## Project structure

The major files for this project are within the `/lib` directory. To understand how the plugin fits together, let's start at the `/lib/types` directory

### /lib/types
The files in this directory all define the types that can be used by GraphQL. For example, there's a `provider` type which describes what information can be available when you make a request for a provider. Each type follows the same pattern defined by the `CustomTypeInterface.php` and `CustomType.php` files. This ensures consistency when defining the types. Each type is registered in `TypeRegister.php`. 

GraphQL understands the following kinds of default scalar types
- Strings
- Booleans
- Integers
- Floats

It can apply these in lists and require them using `non_null` values.

To nest a type e.g. a `Location` type for a clinic or provider, you must register that type and apply it. See the `Provider.php` file as an example.

### /lib/connections and /lib/fields
All of the above just tells graphql that data is available. But it won't show up in an application like GraphiQL just yet. First, you need to register connections and fields. How do you know which one to use? If you want to register a _single_ field name like `provider` and return a single record, register a field. If you want to register a _plural_ field name like `providers` and return multiple records, register a connection.

These files follow a pattern similar to types where there is an interface and base class to ensure consistency. There are also files to register the fields and connections all in one place.

Field and connection files are also where query arguments can be made. These help you filter and refine queries. For instance, the `provider` field has arguments related to `id` and `profileId`. These let you find _specific_ providers. by making queries like:

```
{
  provider (
    profileId: "some-profileID"
  ) {
    firstName
    lastName
  }
}
```

Each of the configs in these files has a `resolve` property should returns either a `singleNodeResolver` or `multipleNodesResolver`. These methods are what _actually_ return the data.

### /lib/resolvers
This is where we do the actual work of making a request to the CFAR API and turning it into a response. In GraphQL land, a resolver is a fancy word for "a function that takes in data and returns data".

The actual requests are made using our Find a Provider SDK. This ensures that we can also cache the responses. There are really only two types of resolvers as defined in the `ResolverInterface.php` and `CustomResolver.php` files. They should do exactly what they say they're going to do...

- `singleNodeResolver`
- `multipleNodesResolver`

The interface and base class don't provide any instructions on _how_ to resolve the methods. They just ensure that the methods are available and have some consistency. In general though, the single node resolver will return a `$result` and the multiple nodes resolver will return an array of nodes in the form `return [ 'nodes' => $nodes ]` (`nodes` is a pretty standard graphql data value for returned arrays). 

## Queries vs Mutations
A query is a request to get data. A mutation is a request to _act_ on some data. Most of this plugin revolves around making queries with one exception. 

### Secure Email
You can make requests to UConn Health's secure email application. This **requires** secure tokens which are created via a separate application. There is a field/resolver set specifically for that purpose. The secure email mutation function will return _either_ an error or success as part of the response. Those values are stored in `/partials/email`.

## TODOS