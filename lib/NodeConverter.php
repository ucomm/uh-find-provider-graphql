<?php

namespace UHFPGraphql;

class NodeConverter
{
  /**
   * GraphQL requires that fields be camelCase rather than PascalCase.
   * This helper method ensures that responses from CFAR comply with that requirement
   *
   * @param array $input - the nodes to convert
   * @return array - an array where all keys are camelCase
   */
  public function resetObjectKeys(array $input = []): array
  {
    return array_map(function ($item) {
      foreach ($item as $key => $value) {
        $item = (array) $item;
        $item[lcfirst($key)] = $value;
        unset($key);
      }
      return $item;
    }, $input);
  }
  /**
   * Filter out providers who should not be publicly visible
   *
   * @param array $nodes - the nodes to convert
   * @return array
   */
  public function onlyVisibleNodes(array $nodes): array
  {
    return array_filter($nodes, function ($node) {
      if ($node['isVisible']) {
        return $node;
      }
    });
  }
}