<?php

namespace UHFPGraphql;

class HTTPRequest {
  public function remoteGet(string $url, bool $increaseTimeout = false, bool $sslVerify = true) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

    if ($increaseTimeout) {
      curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    }

    if (!$sslVerify) {
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    }

    if ($_SERVER['HTTP_HOST'] !== 'localhost') {
      curl_setopt($ch, CURLOPT_SSL_CIPHER_LIST, 'DEFAULT@SECLEVEL=1');
    }

    // Execute request
    $data = curl_exec($ch);
    if (curl_errno($ch)) {
      throw new \Exception(curl_error($ch));
    }
    curl_close($ch);

    $test = json_decode($data) ? json_decode($data) : $data;

    // echo "<pre>";
    // var_dump($test);
    // echo "</pre>";

    return $test;
  }
}