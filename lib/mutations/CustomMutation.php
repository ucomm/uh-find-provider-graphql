<?php

namespace UHFPGraphql\Mutations;

class CustomMutation implements CustomMutationInterface {
  public function __construct(string $type)
  {
    $this->type = $type;
  }

  public function getConfig(): array
  {
    return [];
  }

  public function registerMutation()
  {
    $config = $this->getConfig();
    register_graphql_mutation($this->type, $config);
  }
}