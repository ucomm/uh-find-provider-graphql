<?php

namespace UHFPGraphql\Mutations;

// Mutation example
// use UHFPGraphql\Mutations\MutationType;

class MutationRegister {
  public function __construct()
  {
    array_filter(get_class_methods($this), function ($method) {
      if ($method !== '__construct') {
        add_action('graphql_register_types', [$this, $method]);
      }
    });
  }

  // public function registerMutationExample() {
  //   (new MutationType('typeName'))->registerMutation();
  // }
}