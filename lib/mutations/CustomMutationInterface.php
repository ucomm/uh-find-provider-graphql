<?php

namespace UHFPGraphql\Mutations;

interface CustomMutationInterface {
  public function registerMutation();
  public function getConfig(): array;
}