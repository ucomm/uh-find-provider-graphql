<?php

namespace UHFPGraphql\Types\Clinic;

use UHFPGraphql\Types\CustomType;

class ClinicCategoryEnumType extends CustomType
{
  public function __construct(string $type)
  {
    parent::__construct($type);
  }

  public function getConfig(): array
  {
    return [
      'description' => 'Possible clinic types',
      'values' => [
        'ALL' => [
          'name' => 'All',
          'description' => 'All clinics/default',
          'value' => 'All'
        ],
        'PRIMARY' => [
          'name' => 'Primary',
          'description' => 'Primary care clinics',
          'value' => 'Primary'
        ],
        'SPECIALTY' => [
          'name' => 'Specialty',
          'description' => 'Specialty care clinics',
          'value' => 'Specialty'
        ]
      ]
    ];
  }
}