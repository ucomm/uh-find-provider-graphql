<?php

namespace UHFPGraphql\Types\Clinic;

use UHFPGraphql\Types\CustomType;

class LocationType extends CustomType
{
  public function __construct(string $type)
  {
    parent::__construct($type);
  }

  public function getConfig(): array
  {
    return [
      'description' => 'UConn Health Clinic Location',
      'fields' => [
        'Name' => [
          'type' => 'String',
          'description' => 'Location Name'
        ],
        'SubName' => [
          'type' => 'String',
          'description' => 'Location Sub Name'
        ],
        'Address1' => [
          'type' => 'String',
          'description' => 'Address 1'
        ],
        'Address2' => [
          'type' => 'String',
          'description' => 'Address 2'
        ],
        'Address3' => [
          'type' => 'String',
          'description' => 'Address 3'
        ],
        'Address4' => [
          'type' => 'String',
          'description' => 'Address 4'
        ],
        'City' => [
          'type' => 'String',
          'description' => 'City'
        ],
        'State' => [
          'type' => 'String',
          'description' => 'State'
        ],
        'Zip' => [
          'type' => 'String',
          'description' => 'Zip'
        ],
      ]
    ];
  }
}
