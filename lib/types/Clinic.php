<?php

namespace UHFPGraphql\Types;

class ClinicType extends CustomType
{
  public function __construct(string $type)
  {
    parent::__construct($type);
  }

  public function getConfig(): array
  {
    return [
      'description' => 'UConn Health Clinic',
      'fields' => [
        'LocationName' => [
          'type' => 'String',
          'description' => 'Clinic Location'
        ],
        'LocationSubName' => [
          'type' => 'String',
          'description' => 'Clinic Sub Location'
        ],
        'Address1' => [
          'type' => 'String',
          'description' => 'Address 1'
        ],
        'Address2' => [
          'type' => 'String',
          'description' => 'Address 2'
        ],
        'Address3' => [
          'type' => 'String',
          'description' => 'Address 3'
        ],
        'Address4' => [
          'type' => 'String',
          'description' => 'Address 4'
        ],
        'City' => [
          'type' => 'String',
          'description' => 'City'
        ],
        'State' => [
          'type' => 'String',
          'description' => 'State'
        ],
        'Zip' => [
          'type' => 'String',
          'description' => 'Zip'
        ],
        'Location' => [
          'type' => 'Location',
          'description' => 'Location'
        ],
        'MainTelephoneNumber' => [
          'type' => 'String',
          'description' => 'Main Telephone Number'
        ],
        'AdditionalTelephone1' => [
          'type' => 'String',
          'description' => 'Additional Telephone 1'
        ],
        'AdditionalTelephone2' => [
          'type' => 'String',
          'description' => 'Additional Telephone 2'
        ],
        'LinkToDirections' => [
          'type' => 'String',
          'description' => 'Link to Directions'
        ],
        'SortOrder' => [
          'type' => 'Int',
          'description' => 'Sort Order'
        ]
      ]
    ];
  }
}
