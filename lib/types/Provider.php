<?php

namespace UHFPGraphql\Types;

class ProviderType extends CustomType
{
  public function __construct(string $type)
  {
    parent::__construct($type);
  }

  public function getConfig(): array
  {
    return [
      'description' => 'A UConn Health Provider (Doctor or Dentist)',
      'fields' => [
        'Id' => [
          'type' => 'Int',
          'description' => 'A unique provider ID number'
        ],
        'FirstName' => [
          'type' => 'String',
          'description' => 'The provider\'s first name'
        ],
        'MiddleName' => [
          'type' => 'String',
          'description' => 'The provider\'s middle name'
        ],
        'LastName' => [
          'type' => 'String',
          'description' => 'The provider\'s last name'
        ],
        'Prefix' => [
          'type' => 'String',
          'description' => 'A prefix to the provider\'s name'
        ],
        'Suffix' => [
          'type' => 'String',
          'description' => 'A suffix to the provider\'s name'
        ],
        'Credentials' => [
          'type' => 'String',
          'description' => 'A provider\'s credentials'
        ],
        'Photo' => [
          'type' => 'String',
          'description' => 'A photo as a base64 encoded string'
        ],
        'ProfileId' => [
          'type' => 'String',
          'description' => 'An ID of the provider\'s name - lastName-firstName'
        ],
        'HonorAndAwards' => [
          'type' => 'HonorAndAwards',
          'description' => 'A provider\'s honors and awards'
        ],
        'IsAcceptingPatients' => [
          'type' => 'Boolean',
          'description' => 'Whether or not a provider is accepting patients'
        ],
        'IsVisibleInFacultyDirectory' => [
          'type' => 'Boolean',
          'description' => 'Should the provider be publicly visible in the faculty directory'
        ],
        'IsVisibleInPhysicianDirectory' => [
          'type' => 'Boolean',
          'description' => 'Should the provider be publicly visible in the physician directory'
        ],
        'IsVisibleInDentistDirectory' => [
          'type' => 'Boolean',
          'description' => 'Should the provider be publicly visible in the dentist directory'
        ],
        'Languages' => [
          'type' => [
            'list_of' => 'String'
          ],
          'description' => 'Languages other than English the provider speaks.'
        ],
        'VideoProfileLink' => [
          'type' => 'String',
          'description' => 'A link to the provider\'s video profile'
        ],
        'NPINumber' => [
          'type' => 'String',
          'description' => 'The provider\'s National Physician Id number'
        ]
      ]
    ];
  }
}
