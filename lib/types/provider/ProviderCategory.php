<?php

namespace UHFPGraphql\Types\Provider;

use UHFPGraphql\Types\CustomType;

class ProviderCategoryEnumType extends CustomType {
  public function __construct(string $type)
  {
    parent::__construct($type);
  }

  public function getConfig(): array
  {
    return [
      'description' => 'Possible provider categories',
      'values' => [
        'DOCTOR' => [
          'name' => 'DOCTOR',
          'description' => 'Doctors',
          'value' => 'DOCTOR'
        ],
        'DENTIST' => [
          'name' => 'DENTIST',
          'description' => 'Dentists',
          'value' => 'DENTIST'
        ]
      ]
    ];  
  }
}