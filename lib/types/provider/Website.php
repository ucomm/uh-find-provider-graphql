<?php

namespace UHFPGraphql\Types\Provider;

use UHFPGraphql\Types\CustomType;

class WebsiteType extends CustomType {
  public function __construct(string $type)
  {
    parent::__construct($type);
  }
  public function getConfig(): array
  {
    return [
      'description' => 'A provider\'s website',
      'fields' => [
        'Name' => [
          'type' => 'String',
          'descrption' => 'The website name'
        ],
        'Url' => [
          'type' => 'String',
          'description' => 'The site url'
        ],
        'SortOrder' => [
          'type' => 'Int',
          'description' => 'Sort order'
        ]
      ]
    ];
  }
}