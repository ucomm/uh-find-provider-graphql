<?php

namespace UHFPGraphql\Types\Provider;

use UHFPGraphql\Types\CustomType;

class ProfessionalProfileType extends CustomType
{
  public function __construct(string $type)
  {
    parent::__construct($type);
  }
  public function getConfig(): array
  {
    return [
      'description' => 'A provider\'s professional profile',
      'fields' => [
        'PublicNameTitle' => [
          'type' => 'String',
          'description' => 'The public name of a title'
        ],
        'PublicRankPositions' => [
          'type' => 'String',
          'description' => 'Public rank of positions'
        ],
        'PublicMailingAddress' => [
          'type' => 'String',
          'description' => 'The public mailing address'
        ],
        'PublicContactInformation' => [
          'type' => 'String',
          'description' => 'Public contact information'
        ],
        'PublicEducation' => [
          'type' => 'String',
          'description' => 'Public education information'
        ],
        'ClinicalProfile' => [
          'type' => 'String',
          'description' => 'A clinical profile'
        ],
        'ResearchProfile' => [
          'type' => 'String',
          'description' => 'A research profile'
        ],
        'TeachingProfile' => [
          'type' => 'String',
          'description' => 'A teaching profile'
        ],
        'GeneralProfile' => [
          'type' => 'String',
          'description' => 'A general profile'
        ],
        'Specialties' => [
          'type' => 'String',
          'description' => 'Specialties'
        ],
        'LabRotations' => [
          'type' => 'String',
          'description' => 'Lab rotations'
        ],
        'FacultyRankTitle' => [
          'type' => 'String',
          'description' => 'Faculty title'
        ],
        'ResearchOpportunities' => [
          'type' => 'String',
          'description' => 'Research opportunities'
        ],
      ]
    ];
  }
}
