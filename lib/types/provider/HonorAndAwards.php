<?php

namespace UHFPGraphql\Types\Provider;

use UHFPGraphql\Types\CustomType;

class HonorAndAwardsType extends CustomType {
  public function __construct(string $type)
  {
    parent::__construct($type);
  }

  public function getConfig(): array
  {
    return [
      'description' => 'A provider\'s honors and awards',
      'fields' => [
        'Description' => [
          'type' => 'String',
          'description' => 'Award/honor description'
        ],
        'AwardingOrg' => [
          'type' => 'String',
          'description' => 'The organization that gave the honor or award'
        ],
        'IsVisible' => [
          'type' => 'Boolean',
          'description' => 'Should the award be publicly visible'
        ],
        'YearReceived' => [
          'type' => 'Int',
          'description' => 'Year the award/honor was recieved'
        ]
      ]
    ];
  }
}