<?php

namespace UHFPGraphql\Types\Provider;

use UHFPGraphql\Types\CustomType;

class TrainingType extends CustomType {
  public function __construct(string $type) {
    parent::__construct($type);
  }
  public function getConfig(): array
  {
    return [
      'description' => 'A provider\'s training separate from their education',
      'fields' => [
        'Title' => [
          'type' => 'String',
          'description' => 'The name of the training'
        ],
        'Type' => [
          'type' => 'String',
          'description' => 'The type of training'
        ],
        'TypeId' => [
          'type' => 'Int',
          'description' => 'The ID of the training'
        ],
        'Institution' => [
          'type' => 'String',
          'description' => 'The name of the institution the training took place at'
        ],
        'EndYear' => [
          'type' => 'Int',
          'description' => 'When the training was over'
        ],
        'StartYear' => [
          'type' => 'Int',
          'description' => 'When the training started'
        ]
      ]
    ];
  }
}