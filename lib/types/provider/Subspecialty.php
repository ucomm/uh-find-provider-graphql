<?php

namespace UHFPGraphql\Types\Provider;

use UConnHealth\Specialty;
use UHFPGraphql\Types\CustomType;
use UHFPGraphql\Types\SpecialtyType;

class SubspecialtyType extends CustomType {
  public function __construct(string $type)
  {
    parent::__construct($type);
  }
  public function getConfig(): array
  {

    // subspecialties are almost identical to specialties
    $specialty = new SpecialtyType('Specialty');
    $config = $specialty->getConfig();
    $config['description'] = 'A medical subspecialty';
    $config['fields']['OtherDescription'] = [
      'type' => 'String',
      'description' => 'An alternative description for the subspecialty'
    ];

    return $config;
  }
}