<?php

namespace UHFPGraphql\Types\Provider;

use UHFPGraphql\Types\CustomType;

class LanguageType extends CustomType
{
  public function __construct(string $type)
  {
    parent::__construct($type);
  }
  public function getConfig(): array
  {
    return [
      'description' => 'A language spoken by the provider',
      'fields' => [
        'Description' => [
          'type' => 'String',
          'descrption' => 'The language'
        ],
      ]
    ];
  }
}
