<?php

namespace UHFPGraphql\Types\Provider;

use UHFPGraphql\Types\CustomType;

class BoardSpecialtyType extends CustomType
{
  public function __construct(string $type)
  {
    parent::__construct($type);
  }
  public function getConfig(): array
  {
    return [
      'description' => 'Board certifications/specialties',
      'fields' => [
        'Id' => [
          'type' => 'Int',
          'description' => 'The specialty ID'
        ],
        'SpecialtyTypeId' => [
          'type' => 'Int',
          'description' => 'The specialty type ID'
        ],
        'OtherSpecialtyBoard' => [
          'type' => 'String',
          'description' => 'Other board specialties'
        ],
        'CertificationSpecialtyTypeId' => [
          'type' => 'Int',
          'description' => 'The certification specialty type ID'
        ],
        'CertificationSpecialtyType' => [
          'type' => 'String',
          'description' => 'The certification specialty type'
        ],
        'OtherSpecialtyCertification' => [
          'type' => 'String',
          'description' => 'Other certification'
        ],
        'YearGranted' => [
          'type' => 'Int',
          'description' => 'The year the certification was granted'
        ],
        'YearExpired' => [
          'type' => 'Int',
          'description' => 'The year the certification expired'
        ],
        'Description' => [
          'type' => 'String',
          'description' => 'Description'
        ],
      ]
    ];
  }
}