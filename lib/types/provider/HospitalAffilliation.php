<?php

namespace UHFPGraphql\Types\Provider;

use UHFPGraphql\Types\CustomType;

class HospitalAffilliationType extends CustomType
{
  public function __construct(string $type)
  {
    parent::__construct($type);
  }
  public function getConfig(): array
  {
    return [
      'description' => 'A provider\'s afflliation outside of UConn Health',
      'fields' => [
        'Description' => [
          'type' => 'String',
          'descrption' => 'The affilliation description'
        ],
      ]
    ];
  }
}
