<?php

namespace UHFPGraphql\Types\Provider;

use UHFPGraphql\Types\CustomType;

class FinancialDisclosureType extends CustomType {
  public function __construct(string $type)
  {
    parent::__construct($type);
  }

  public function getConfig(): array
  {
    return [
      'description' => 'Information about a provider\'s conflicts of interest',
      'fields' => [
        'FinancialDisclosureData' => [
          'type' => 'String',
          'description' => 'Raw HTML representing any conflict of interest disclosure information'
        ],
        'UpdatedDate' => [
          'type' => 'String',
          'description' => 'A timestamp with the date of the last update for the conflict of interest data'
        ]
      ]
    ];
  }
}