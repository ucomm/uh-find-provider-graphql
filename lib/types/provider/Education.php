<?php

namespace UHFPGraphql\Types\Provider;

use UHFPGraphql\Types\CustomType;

class EducationType extends CustomType {
  public function __construct(string $type) {
    parent::__construct($type);
  }
  public function getConfig(): array
  {
    return [
      'description' => 'A provider\'s education',
      'fields' => [
        'Id' => [
          'type' => 'Int',
          'description' => 'A numeric ID'
        ],
        'DegreeId' => [
          'type' => 'Int',
          'description' => 'The ID of a Degree'
        ],
        'Degree' => [
          'type' => 'String',
          'descrption' => 'An earned degree'
        ],
        'StartYear' => [
          'type' => 'Int',
          'description' => 'Year the degree began'
        ],
        'EndYear' => [
          'type' => 'Int',
          'description' => 'Year the degree ended'
        ],
        'HonorId' => [
          'type' => 'Int',
          'description' => 'Numeric ID of the degree'
        ],
        'Honor' => [
          'type' => 'String',
          'description' => 'The type of degree'
        ],
        'Other' => [
          'type' => 'String',
          'description' => 'Other information'
        ],
        'City' => [
          'type' => 'String',
          'description' => 'City'
        ],
        'Country' => [
          'type' => 'String',
          'description' => 'Country'
        ],
        'Training' => [
          'type' => 'String',
          'description' => 'Training'
        ],
        'Institution' => [
          'type' => 'String',
          'description' => 'The institution that awarded the degree'
        ],
        'CategoryId' => [
          'type' => 'Int',
          'description' => 'Numeric ID of the category'
        ],
        'Category' => [
          'type' => 'String',
          'description' => 'The category of the degree'
        ],
        'Major' => [
          'type' => 'String',
          'description' => 'The major area of study for the degree'
        ],
        'Minor' => [
          'type' => 'String',
          'description' => 'The minor area of study for the degree'
        ],
        'IsVisible' => [
          'type' => 'Bool',
          'description' => 'If the degree should be publicly visible'
        ]
      ]
    ];
  }
}