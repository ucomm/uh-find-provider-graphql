<?php

namespace UHFPGraphql\Types;

class SecureEmailToken extends CustomType {
  public function __construct(string $type)
  { 
    parent::__construct($type);
  }

  public function getConfig(): array
  {
    return [
      'description' => 'UConn Health Secure Email Token',
      'fields' => [
        'id' => [
          'type' => 'String',
          'description' => 'An ID based on the token'
        ],
        'token' => [
          'type' => 'String',
          'description' => 'Token'
        ]
      ]
    ];
  }
}