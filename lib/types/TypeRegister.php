<?php

namespace UHFPGraphql\Types;

use UHFPGraphql\Types\Clinic\ClinicCategoryEnumType;
use UHFPGraphql\Types\Clinic\LocationType;
use UHFPGraphql\Types\Provider\BoardSpecialtyType;
use UHFPGraphql\Types\Provider\ProviderCategoryEnumType;
use UHFPGraphql\Types\Provider\EducationType;
use UHFPGraphql\Types\Provider\FinancialDisclosureType;
use UHFPGraphql\Types\Provider\HonorAndAwardsType;
use UHFPGraphql\Types\Provider\HospitalAffilliationType;
use UHFPGraphql\Types\Provider\ProfessionalProfileType;
use UHFPGraphql\Types\Provider\SubspecialtyType;
use UHFPGraphql\Types\Provider\TrainingType;
use UHFPGraphql\Types\Provider\WebsiteType;
use UHFPGraphql\Types\SecureEmailToken;

class TypeRegister
{
  public function __construct()
  {
    array_filter(get_class_methods($this), function($method) {
      if ($method !== '__construct') {
        add_action('graphql_register_types', [$this, $method]);
      }
    });
  }

  public function registerClinicType()
  {
    (new ClinicType('Clinic'))->registerType();
  }

  public function registerClinicCategoryEnumType()
  {
    (new ClinicCategoryEnumType('ClinicCategoryEnum'))->registerEnumType();
  }

  public function registerBoardSpecialtyType()
  {
    (new BoardSpecialtyType('BoardSpecialty'))->registerType();
  }

  public function registerEducationType()
  {
    (new EducationType('Education'))->registerType();
  }

  public function registerFinancialDisclosureType() {
    (new FinancialDisclosureType('FinancialDisclosure'))->registerType();
  }

  public function registerHonorAndAwardsType()
  {
    (new HonorAndAwardsType('HonorAndAwards'))->registerType();
  }

  public function registerHospitalAffilliationType()
  {
    (new HospitalAffilliationType('HospitalAffilliation'))->registerType();
  }

  public function registerTrainingType()
  {
    (new TrainingType('Training'))->registerType();
  }

  public function registerProfessionalProfileType()
  {
    (new ProfessionalProfileType('ProfessionalProfile'))->registerType();
  }

  public function registerWebsiteType()
  {
    (new WebsiteType('Website'))->registerType();
  }

  public function registerLocationType()
  {
    (new LocationType('Location'))->registerType();
  }

  public function registerProviderType()
  {
    (new ProviderType('Provider'))->registerType();
  }

  public function registerProviderCategoryEnumType()
  {
    (new ProviderCategoryEnumType('ProviderCategoryEnum'))->registerEnumType();
  }

  public function registerSubSpecialtyType()
  {
    (new SubspecialtyType('Subspecialty'))->registerType();
  }

  public function registerSpecialtyType()
  {
    (new SpecialtyType('Specialty'))->registerType();
  }

  public function registerSecureEmailTokenType() {
    (new SecureEmailToken('SecureEmailToken'))->registerType();
  }
}