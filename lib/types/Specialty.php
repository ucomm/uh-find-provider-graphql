<?php

namespace UHFPGraphql\Types;

class SpecialtyType extends CustomType
{
  public function __construct(string $type)
  {
    parent::__construct($type);
  }

  public function getConfig(): array
  {
    return [
      'description' => 'Provider Specialties',
      'fields' => [
        'Id' => [
          'type' => 'Int',
          'description' => 'The specialty ID'
        ],
        'Name' => [
          'type' => 'String',
          'description' => 'The specialty name'
        ],
        'Link' => [
          'type' => 'String',
          'description' => 'Link to the specialty'
        ],
        'IsVisible' => [
          'type' => 'Bool',
          'description' => 'Is the specialty publicly visible?'
        ],
        'SortOrder' => [
          'type' => 'Int',
          'description' => 'Sort order'
        ]
      ]
    ];
  }
}
