<?php

namespace UHFPGraphql\Resolvers;

class CustomResolver implements ResolverInterface {

  protected $isDevEnv;
  protected $policies;

  public function __construct()
  {

    $this->isDevEnv = $_SERVER['HTTP_HOST'] !== 'health.uconn.edu' ? true : false;
    $this->policies = [
      'useCache' => true,
      'useDevEndpoint' => false
    ];
  }

  public function singleNodeResolver($root, $args, $context, $info)
  {
    return false;
  }

  public function multipleNodesResolver($root, $args, $context, $info): array
  {
    return [];
  }


  protected function setSingleNodeDevPolicies(array $args): array {
    
    if ($this->isDevEnv) {
      $this->policies['useCache'] = isset($args['useCache']) ? $args['useCache'] : false;
      $this->policies['useDevEndpoint'] = isset($args['devEndpoint']) ? $args['devEndpoint'] : false;
    }

    return $this->policies;
  }

  protected function setMultiNodesDevPolicies(array $args): array {
    
    if ($this->isDevEnv) {
      $this->policies['useCache'] = isset($args['where']['useCache']) ? $args['where']['useCache'] : false;
      $this->policies['useDevEndpoint'] = isset($args['where']['devEndpoint']) ? $args['where']['devEndpoint'] : false;
    }

    return $this->policies;
  }

}