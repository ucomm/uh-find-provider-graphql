<?php

namespace UHFPGraphql\Resolvers;

use UHFPGraphql\NodeConverter;
use UConnHealth\RequestService;

class ClinicResolver extends CustomResolver {

  public function multipleNodesResolver($root, $args, $context, $info): array {
    $response = [];

    $this->setMultiNodesDevPolicies($args);

    $useCache = $this->policies['useCache'];
    $useDevEndpoint = $this->policies['useDevEndpoint'];

    $converter = new NodeConverter();

    if (isset($args['where']['category'])) {
      $response = RequestService::getClinicsByCategory($args['where']['category'], $useCache, $useDevEndpoint);
    } else {
      $response = RequestService::getClinics($useCache, $useDevEndpoint);
    }

    $nodes = $converter->resetObjectKeys($response->clinics);

    if (isset($args['where']['city'])) {
      $nodes = array_filter($nodes, function ($node) use ($args) {
        if ($node['city'] === $args['where']['city']) {
          return $node;
        }
      });
    }

    if (isset($args['where']['locationName'])) {
      $nodes = array_filter($nodes, function ($node) use ($args) {
        if ($node['locationName'] === $args['where']['locationName']) {
          return $node;
        }
      });
    }

    return [
      'nodes' => $nodes
    ];
  }
}

