<?php

namespace UHFPGraphql\Resolvers\Provider;

use UHFPGraphql\Resolvers\CustomResolver;
use UHFPGraphql\NodeConverter;

class ProfessionalProfileResolver extends CustomResolver
{
  public function singleNodeResolver($root, $args, $context, $info)
  {
    $profile = (array)$root['professionalProfile'];

    $converted = [];

    foreach ($profile as $key => $value) {
      $converted[lcfirst($key)] = $value;
    }

    return $converted;
  }

  public function multipleNodesResolver($root, $args, $context, $info): array
  {
    $converter = new NodeConverter();

    $is_visible_arg = isset($args['where']['isVisible']) &&
      $args['where']['isVisible'] === true
      ? true : false;

    $nodes = $converter->resetObjectKeys($root[$context->fieldName] ?? []);

    if ($is_visible_arg) {
      $nodes = $converter->onlyVisibleNodes($nodes);
    }
    return ['nodes' => $nodes];
  }
}
