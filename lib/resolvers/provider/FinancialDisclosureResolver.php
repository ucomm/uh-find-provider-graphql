<?php

namespace UHFPGraphql\Resolvers\Provider;

use UHFPGraphql\Resolvers\CustomResolver;
use UHFPGraphql\NodeConverter;

class FinancialDisclosureResolver extends CustomResolver {
  public function singleNodeResolver($root, $args, $context, $info)
  {
    $financialDisclosure = (array)$root['financialDisclosure'];
    $converted = [];
    foreach ($financialDisclosure as $key => $value) {
      $converted[lcfirst($key)] = $value;
    }
    return $converted;
  }
}