<?php

namespace UHFPGraphql\Resolvers\Provider;

use UHFPGraphql\Resolvers\CustomResolver;
use UHFPGraphql\NodeConverter;

class ConnectionResolver extends CustomResolver {
  public function multipleNodesResolver($root, $args, $context, $info): array
  {
    $converter = new NodeConverter();

    $is_visible_arg = isset($args['where']['isVisible']) &&
      $args['where']['isVisible'] === true
      ? true : false;

    $nodes = $converter->resetObjectKeys($root[$context->fieldName] ?? []);
    
    if ($is_visible_arg) {
      $nodes = $converter->onlyVisibleNodes($nodes);
    }
    return [ 'nodes' => $nodes ];
  }
}