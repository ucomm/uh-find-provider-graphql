<?php

namespace UHFPGraphql\Resolvers;

use stdClass;
use UConnHealth\ProviderCollection;
use UConnHealth\RequestService;
use UHFPGraphql\HTTPRequest;
use UHFPGraphql\Resolvers\CustomResolver;
use UHFPGraphql\NodeConverter;

class ProviderResolver extends CustomResolver {

  public function singleNodeResolver($root, $args, $context, $info)
  {
    $policies = $this->setSingleNodeDevPolicies($args);
    $result = null;
    $useCache = $policies['useCache'];
    $useDevEndpoint = $policies['useDevEndpoint'];

    if (isset($args['id'])) {
      $result = RequestService::getProviderById($args['id'], $useCache);
    } else if (isset($args['profileId'])) {
      $result = RequestService::getProviderByProfileId($args['profileId'], $useCache, $useDevEndpoint);
    }

    $result = (array)$result;

    foreach ($result as $key => $value) {
      $new_key = lcfirst($key);
      $result[$new_key] = $value;
      unset($result[$key]);
    }

    return $result;
  }

  public function multipleNodesResolver($root, $args, $context, $info): array
  {
    $policies = $this->setMultiNodesDevPolicies($args);
    $converter = new NodeConverter();
    $response = [];
    $useCache = $policies['useCache'];
    $useDevEndpoint = $policies['useDevEndpoint'];

    $use_parameters = $args['where']['useParameters'];
    $use_last_name = isset($args['where']['lastName']) && $args['where']['lastName'] !== '' ? true : false;
    $use_location = isset($args['where']['location']) && $args['where']['location'] !== '' ? true : false;
    $fetch_all = isset($args['where']['fetchAll']) && $args['where']['fetchAll'] === true ? true : false;

    if ($use_parameters) {
      $service = new ProviderCollection();

      if ($args['where']['category'] === 'DENTIST') {
        $service->setDentist();
      } else {
        $service->setDoctor();
      }

      $service->enableWpCache();
      $service->addSpecialties($args['where']['specialties']);

      if ($use_last_name) {
        $service->lastname = $args['where']['lastName'];
      }

      if ($use_location) {
        $service->setLocation($args['where']['location']);
      }

      $service->fetchProviders();
      $response = $service->providers;

      $nodes = $converter->resetObjectKeys($response);
    } else if (!$use_parameters && $fetch_all) {

      $transient_string = 'fap-all-providers';

      if (!get_transient($transient_string)) {
        // try to limit the number of times this runs.
        // we don't want it somehow ending up in an infinite loop if there's something wrong with the API
        $attempts = 0;
        $transient_set = false;
        do {
          $response = $this->getAllProviders();
          $nodes = $this->createNodes($response);
          $transient_set = $this->setTransient($transient_string, $nodes);
          $attempts++;
        } while (!$transient_set && $attempts < 5);
      } else {
        $nodes = get_transient($transient_string);
      }
    } else {
      $response = RequestService::getProvidersByLastName($args['where']['lastName'], $useCache, $useDevEndpoint);
      $nodes = $converter->resetObjectKeys($response->providers);
    }

    return [
      'nodes' => $nodes
    ];
  }

  /**
   * 
   * this is a HUGE response so sometimes it takes longer than the default 5 seconds
   * in testing a non-cached request to this endpoint can take up to 20 seconds
   * 
   */
  private function getAllProviders() {
    $request = new HTTPRequest();
    return $request->remoteGet('https://publicdirectoryapi.uchc.edu/api/Providers/all', true);
  }

  /**
   * Create an array of nodes to return in the graphql response object.
   *
   * @param $response either a wordpress error object or success response
   * @return array
   */
  private function createNodes($response): array {
    if (is_wp_error($response)) {
      return [];
    }

    return array_map(function ($provider) {
      $new_provider = new stdClass();
      $new_provider->firstName = $provider->FirstName;
      $new_provider->middleName = $provider->MiddleName;
      $new_provider->lastName = $provider->LastName;
      $new_provider->credentials = $provider->Credentials;
      $new_provider->profileId = $provider->ProfileId;

      return $new_provider;
    }, $response);
  }

  /**
   * Count the array and attempt to create a cached result
   *
   * @param string $transient_string
   * @param array $nodes - the array of providers/data to cache
   * @return boolean
   */
  private function setTransient(string $transient_string, array $nodes): bool {
    return count($nodes) > 0 ?
      set_transient($transient_string, $nodes, DAY_IN_SECONDS) :
      false;
  }
}