<?php

namespace UHFPGraphql\Resolvers;

use UConnHealth\RequestService;
use UHFPGraphql\NodeConverter;

class SpecialtyResolver extends CustomResolver {
  public function multipleNodesResolver($root, $args, $context, $info): array
  {
    $policies = $this->setMultiNodesDevPolicies($args);
    $useCache = $policies['useCache'];
    $useDevEndpoint = $policies['useDevEndpoint'];

    $is_visible_arg = isset($args['where']['isVisible']) && 
      $args['where']['isVisible'] === true 
      ? true : false;

    $converter = new NodeConverter();

    $provider_type = $args['where']['category'] === 'DOCTOR' ?
      'doctor' : 'dentist';

    $response = null;

    if ($provider_type === 'doctor') {
      $response = RequestService::getDoctorSpecialties($useCache, $useDevEndpoint);
    } else {
      $response = RequestService::getDentistSpecialties($useCache, $useDevEndpoint);
    }

    $nodes = $converter->resetObjectKeys($response->specialties);

    if ($is_visible_arg) {
      $nodes = $converter->onlyVisibleNodes($nodes);
    }

    if (isset($args['where']['names'])) {
      $nodes = array_filter($nodes, function($node) use ($args) {
        if (in_array($node['name'], $args['where']['names'])) {
          return $node;
        }
      });
    }

    return [
      'nodes' => $nodes
    ];
  }

  // /**
  //  * Returns an array of specialties for a specific provider
  //  *
  //  * @param mixed $root - The previous object or array being resolved.
  //  * @param array $args - The field's arguments if any exist.
  //  * @param AppContext $context - Used for things like DataLoading/caching
  //  * @param ResolveInfo $info - Used to determine: where the field should be, parent type, etc
  //  * @return array
  //  */
  // public function multipleNodesProviderResolver($root, $args, $context, $info): array {
  //   $converter = new NodeConverter();

  //   $nodes = $converter->resetObjectKeys($root['specialties']);

  //   if (isset($args['where']['isVisible'])) {
  //     $nodes = $converter->onlyVisibleNodes($nodes);
  //   }

  //   return [
  //     'nodes' => $nodes
  //   ];
  // }
}