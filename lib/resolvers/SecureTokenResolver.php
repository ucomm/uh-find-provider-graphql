<?php

namespace UHFPGraphql\Resolvers;

use UHFPGraphql\HTTPRequest;

class SecureTokenResolver extends CustomResolver {

  private $generator_url = 'https://test-mailformprocessor.uchc.edu/GenKey.aspx?';

  private function getGeneratorUrl() {
    if ($_SERVER['HTTP_HOST'] === "health.uconn.edu") {
      $this->generator_url = 'https://mailformprocessor.uchc.edu/GenKey.aspx?';
    }
    return $this->generator_url;
  }

  public function singleNodeResolver($root, $args, $context, $info)
  {
    $url = $this->getGeneratorUrl();
    
    $params = [
      'MailTo' => 'info@uchc.edu',
      'MailSubject' => 'Form Mailer',
      'MailFrom' => 'webmaster@uconn.edu',
      'RedirectURL' => UHFP_GRAPHQL_URL . 'partials/email/success.php',
      'ErrorURL' => UHFP_GRAPHQL_URL . 'partials/email/error.php'
    ];

    if (isset($args['mailFrom']) && $args['mailFrom'] !== '') {
      $params['MailFrom'] = $args['mailFrom'];
    }

    if (isset($args['mailTo']) && $args['mailTo'] !== '') {
      $params['MailTo'] = $args['mailTo'];
    } 

    if (isset($args['redirectURL']) && $args['redirectURL'] !== '') {
      $params['RedirectURL'] = $args['redirectURL'];
    }

    if (isset($args['errorURL']) && $args['errorURL'] !== '') {
      $params['ErrorURL'] = $args['errorURL'];
    }

    $encoded_params = http_build_query($params);

    $request = new HTTPRequest();
    $token_request = $request->remoteGet($url . $encoded_params, false, false);

    // echo "<pre>";
    // var_dump($token_request);
    // echo "</pre>";

    // further mangle the response and get the first 6 characters as a unique ID
    // the ID is helpful for the Apollo framework
    $id = substr(md5($token_request), 0, 6);

    return [
      'id' =>  $id,
      'token' => $token_request
    ];
  }
}