<?php

namespace UHFPGraphql\Resolvers;

interface ResolverInterface {
  /**
   * Returns a single node from a query
   *
   * @param mixed $root - The previous object or array being resolved.
   * @param array $args - The field's arguments if any exist.
   * @param AppContext $context - Used for things like DataLoading/caching
   * @param ResolveInfo $info - Used to determine: where the field should be, parent type, etc
   * @return void
   */
  public function singleNodeResolver($root, $args, $context, $info);

  /**
   * Returns an array of nodes from a query
   *
   * @param mixed $root - The previous object or array being resolved.
   * @param array $args - The field's arguments if any exist.
   * @param AppContext $context - Used for things like DataLoading/caching
   * @param ResolveInfo $info - Used to determine: where the field should be, parent type, etc
   * @return array
   */
  public function multipleNodesResolver($root, $args, $context, $info): array;
}