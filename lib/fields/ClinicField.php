<?php

namespace UHFPGraphql\Fields;

use UHFPGraphql\HTTPRequest;

class ClinicField extends CustomField {
  public function __construct()
  {
    parent::__construct('RootQuery', 'clinic');
  }

  protected function setConfig(): array {
    return [
      'decription' => 'A single clinic',
      'type' => 'Clinic',
      'resolve' => function ($root, $args, $context, $info) {
        $request = new HTTPRequest();
        return $request->remoteGet(FIND_PROVIDER_API_URL . '/Providers/clinics');
      }
    ];
  }
}