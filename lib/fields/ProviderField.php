<?php

namespace UHFPGraphql\Fields;

use UHFPGraphql\Resolvers\ProviderResolver;

class ProviderField extends CustomField {
  public function __construct()
  {
    parent::__construct('RootQuery', 'provider');
  }

  protected function setConfig(): array
  {

    $args = [
      'id' => [
        'type' => 'Int',
        'description' => 'A provider\'s numeric ID'
      ],
      'profileId' => [
        'type' => 'String',
        'description' => 'A provider\'s profile ID in the format lastName-firstName'
      ],
      'devEndpoint' => [
        'type' => 'Bool',
        'description' => 'For testing only - Use the CFAR test API'
      ],
      'useCache' => [
        'type' => 'Bool',
        'description' => 'For testing only - toggle cache behavior for requests'
      ]
    ];

    /**
     * 
     * the args array here lets you get a single provider by their numeric ID or profileId
     * 
     */
    return [
      'description' => 'A single UConn Health Provider (Doctor/Dentist)',
      'type' => 'Provider',
      'args' => $args,
      'resolve' => function ($root, $args, $context, $info) {
        return (new ProviderResolver)->singleNodeResolver($root, $args, $context, $info);
      }
    ];
  }
}