<?php

namespace UHFPGraphql\Fields;

use UHFPGraphql\Resolvers\Provider\FinancialDisclosureResolver;

class FinancialDisclosureField extends CustomField {
  public function __construct()
  {
    parent::__construct('Provider', 'financialDisclosure');
  }

  protected function setConfig(): array
  {
    return [
      'description' => 'A provider\'s financial disclosure/conflict of interest data',
      'type' => 'FinancialDisclosure',
      'resolve' => function ($root, $args, $context, $info) {
        $context->fieldName = 'financialDisclosure';
        return (new FinancialDisclosureResolver)->singleNodeResolver($root, $args, $context, $info);
      }
    ];
  }
}