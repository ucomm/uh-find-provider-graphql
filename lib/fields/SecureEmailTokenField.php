<?php

namespace UHFPGraphql\Fields;

use UHFPGraphql\Resolvers\SecureTokenResolver;

class SecureTokenField extends CustomField {
  public function __construct()
  {
    parent::__construct('RootQuery', 'uhSecureToken');
  }

  protected function setConfig(): array
  {
    return [
      'description' => 'A token for secure UConn Health email',
      'type' => 'SecureEmailToken',
      'args' => [
        'mailFrom' => [
          'type' => 'String',
          'description' => 'The email address to use as a mail from param. The default is dataFormHandler@uchc.edu'
        ],
        'mailTo' => [
          'type' => 'String',
          'description' => 'The email address to use as a mail to param. The default is info@uchc.edu'
        ],
        'redirectURL' => [
          'type' => 'String',
          'description' => 'The URL of the success response. Should go to a php file'
        ],
        'errorURL' => [
          'type' => 'String',
          'description' => 'The URL of the error response. Should go to a php file'
        ]
      ],
      'resolve' => function ($root, $args, $context, $info) {
        return (new SecureTokenResolver)->singleNodeResolver($root, $args, $context, $info);
      }
    ];
  }
}