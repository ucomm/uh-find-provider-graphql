<?php

namespace UHFPGraphql\Fields;

use UHFPGraphql\Resolvers\Provider\ProfessionalProfileResolver;

class ProfessionalProfileField extends CustomField {
  public function __construct()
  {
    parent::__construct('Provider', 'professionalProfile');
  }

  protected function setConfig(): array
  {
    return [
      'description' => 'A provider\'s professional profile',
      'type' => 'ProfessionalProfile',
      'resolve' => function ($root, $args, $context, $info) {
        $context->fieldName = 'professionalProfile';
        return (new ProfessionalProfileResolver)->singleNodeResolver($root, $args, $context, $info);
      }
    ];
  }
}