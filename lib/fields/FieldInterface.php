<?php

namespace UHFPGraphql\Fields;

interface FieldInterface {
  public function registerField();
}