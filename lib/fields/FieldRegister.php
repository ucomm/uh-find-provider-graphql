<?php

namespace UHFPGraphql\Fields;

class FieldRegister {
  public function __construct()
  {
    array_filter(get_class_methods($this), function ($method) {
      if ($method !== '__construct') {
        add_action('graphql_register_types', [$this, $method], 99);
      }
    });
  }

  public function registerClinicField() {
    (new ClinicField)->registerField();
  }

  public function registerFinancialDisclosureField() {
    (new FinancialDisclosureField)->registerField();
  }

  public function registerProfessionalProfileField() {
    (new ProfessionalProfileField)->registerField();
  }

  public function registerProviderField() {
    (new ProviderField)->registerField();
  }

  public function registerSecureTokenField() {
    (new SecureTokenField)->registerField();
  }
}