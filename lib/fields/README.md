These files register _single_ fields e.g. provider, clinic, etc... If you need to resolve a single thing from a response do it in the `resolve` function on the field with a `singleNodeResolver`.

DO NOT try to resolve a single thing from the connections files.