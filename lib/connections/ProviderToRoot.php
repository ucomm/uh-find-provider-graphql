<?php

namespace UHFPGraphql\Connections;

use UHFPGraphql\Resolvers\ProviderResolver;

class ProviderToRoot extends CustomConnection {
  protected function getConfig(): array
  {

    $providerArgs = [
      'useParameters' => [
        'type' => [
          'non_null' => 'Bool'
        ],
        'description' => 'Set the endpoint to use. If false, the query will only search against last names. If true, the query will search using the provider by parameters endpoint.'
      ],
      'fetchAll' => [
        'type' => 'Bool',
        'description' => 'Get information about all providers in the system. Default is false'
      ],
      'category' => [
        'type' => 'ProviderCategoryEnum',
        'description' => 'Search for either doctors or dentists'
      ],
      'specialties' => [
        'type' => [
          'list_of' => 'String',
          'description' => 'A list of possible specialties to search against'
        ]
      ],
      'lastName' => [
        'type' => 'String',
        'description' => 'Search for providers by their last name'
      ],
      'location' => [
        'type' => 'String',
        'description' => 'A location'
      ],
    ];

    $providerArgs = array_merge($this->connectionArgs, $providerArgs);

    return [
      'fromType' => 'RootQuery',
      'toType' => 'Provider',
      'fromFieldName' => 'providers',
      'connectionArgs' => $providerArgs,
      'resolve' => function ($root, $args, $context, $info) {
        return (new ProviderResolver)->multipleNodesResolver($root, $args, $context, $info);
      }
    ];
  }
}