<?php

namespace UHFPGraphql\Connections;

use UHFPGraphql\Resolvers\ClinicResolver;

class ClinicToRoot extends CustomConnection {

  protected function getConfig(): array {

    $clinicArgs = [
        'category' => [
          'type' => 'ClinicCategoryEnum',
          'description' => 'Clinic Category Type: All, Primary, Specialty'
        ],
        'city' => [
          'type' => 'String',
          'description' => 'The Clinic City'
        ],
        'locationName' => [
          'type' => 'String',
          'description' => 'The Clinic\'s Name'
        ]
    ];

    $clinicArgs = array_merge($this->connectionArgs, $clinicArgs);

    return [
      'fromType' => 'RootQuery',
      'toType' => 'Clinic',
      'fromFieldName' => 'clinics',
      'resolve' => function($id, $args, $context, $info) {
        return (new ClinicResolver)->multipleNodesResolver($id, $args, $context, $info);
      },
      'connectionArgs' => $clinicArgs
    ];
  }
}