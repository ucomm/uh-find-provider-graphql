<?php

namespace UHFPGraphql\Connections\Provider;

use UHFPGraphql\Connections\CustomConnection;
use UHFPGraphql\Resolvers\Provider\ConnectionResolver;

class ProfessionalProfileToProvider extends CustomConnection
{
  protected function getConfig(): array
  {
    return [
      'fromType' => 'Provider',
      'toType' => 'ProfessionalProfile',
      'fromFieldName' => 'professionalProfile',
      'resolve' => function ($root, $args, $context, $info) {
        $context->fieldName = 'professionalProfile';
        return (new ConnectionResolver)->multipleNodesResolver($root, $args, $context, $info);
      }
    ];
  }
}
