<?php

namespace UHFPGraphql\Connections\Provider;

use UHFPGraphql\Connections\CustomConnection;
use UHFPGraphql\Resolvers\Provider\ConnectionResolver;

class EducationToProvider extends CustomConnection {
  protected function getConfig(): array
  {
    return [
      'fromType' => 'Provider',
      'toType' => 'Education',
      'fromFieldName' => 'education',
      'connectionArgs' => [
        'isVisible' => [
          'type' => 'Boolean',
          'description' => 'Should the degree be publicly visible?'
        ]
      ],
      'resolve' => function($root, $args, $context, $info) {
        $context->fieldName = 'education';
        return (new ConnectionResolver)->multipleNodesResolver($root, $args, $context, $info);
      }
    ];
  }
}