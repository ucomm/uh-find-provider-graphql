<?php

namespace UHFPGraphql\Connections\Provider;

use UHFPGraphql\Connections\CustomConnection;
use UHFPGraphql\Resolvers\Provider\ConnectionResolver;

class ClinicsToProvider extends CustomConnection {
  protected function getConfig(): array
  {
    return [
      'fromType' => 'Provider',
      'toType' => 'Clinic',
      'fromFieldName' => 'clinics',
      'connectionArgs' => [
        'isVisible' => [
          'type' => 'Boolean',
          'description' => 'Should the specialty be publicly visible'
        ]
      ],
      'resolve' => function ($root, $args, $context, $info) {
        $context->fieldName = 'clinics';
        return (new ConnectionResolver)->multipleNodesResolver($root, $args, $context, $info);
      }
    ];
  }
}