<?php

namespace UHFPGraphql\Connections\Provider;

use UHFPGraphql\Connections\CustomConnection;
use UHFPGraphql\Resolvers\Provider\ConnectionResolver;

class TrainingToProvider extends CustomConnection
{
  protected function getMultipleConfig(): array
  {

    $training_types = [
      'residencies',
      'fellowships',
      'internships',
      'postDocTraining',
      'professionalTraining',
      'otherTraining'
    ];

    return array_map(function($type) {
      return [
        'fromType' => 'Provider',
        'toType' => 'Training',
        'fromFieldName' => $type,
        'resolve' => function ($root, $args, $context, $info) use ($type) {
          $context->fieldName = $type;
          return (new ConnectionResolver)->multipleNodesResolver($root, $args, $context, $info);
        }
      ];
    }, $training_types);
  }
}