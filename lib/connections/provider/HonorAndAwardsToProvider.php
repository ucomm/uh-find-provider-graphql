<?php

namespace UHFPGraphql\Connections\Provider;

use UHFPGraphql\Connections\CustomConnection;
use UHFPGraphql\Resolvers\Provider\ConnectionResolver;

class HonorAndAwardsToProvider extends CustomConnection {
  protected function getConfig(): array
  {
    return [
      'fromType' => 'Provider',
      'toType' => 'HonorAndAwards',
      'fromFieldName' => 'honorAndAwards',
      'connectionArgs' => [
        'isVisible' => [
          'type' => 'Boolean',
          'description' => 'Is the award publicly visible?'
        ]
      ],
      'resolve' => function ($root, $args, $context, $info) {
        $context->fieldName = 'honorAndAwards';
        return (new ConnectionResolver)->multipleNodesResolver($root, $args, $context, $info);
      }
    ];
  }
}