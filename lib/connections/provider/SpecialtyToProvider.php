<?php

namespace UHFPGraphql\Connections\Provider;

use UHFPGraphql\Connections\CustomConnection;
use UHFPGraphql\Resolvers\Provider\ConnectionResolver;

class SpecialtyToProvider extends CustomConnection {
  protected function getConfig(): array
  {
    return [
      'fromType' => 'Provider',
      'toType' => 'Specialty',
      'fromFieldName' => 'specialties',
      'connectionArgs' => [
        'isVisible' => [
          'type' => 'Boolean',
          'description' => 'Should the specialty be publicly visible'
        ]
      ],
      'resolve' => function($root, $args, $context, $info) {
        $context->fieldName = 'specialties';
        return (new ConnectionResolver)->multipleNodesResolver($root, $args, $context, $info);
      }
    ];
  }
}