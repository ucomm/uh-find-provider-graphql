<?php

namespace UHFPGraphql\Connections\Provider;

use UHFPGraphql\Connections\CustomConnection;
use UHFPGraphql\Resolvers\Provider\ConnectionResolver;

class SubspecialtyToProvider extends CustomConnection {
  protected function getConfig(): array
  {
    return [
      'fromType' => 'Provider',
      'toType' => 'Subspecialty',
      'fromFieldName' => 'subspecialties',
      'connectionArgs' => [
        'isVisible' => [
          'type' => 'Boolean',
          'description' => 'Should the subspecialty be publicly visible?'
        ]
      ],
      'resolve' => function($root, $args, $context, $info) {
        $context->fieldName = 'subspecialties';
        return (new ConnectionResolver)->multipleNodesResolver($root, $args, $context, $info);
      }
    ];
  }
}
