<?php

namespace UHFPGraphql\Connections;

use UHFPGraphql\Connections\Provider\BoardSpecialtiesToProvider;
use UHFPGraphql\Connections\Provider\ClinicsToProvider;
use UHFPGraphql\Connections\Provider\EducationToProvider;
use UHFPGraphql\Connections\Provider\HonorAndAwardsToProvider;
use UHFPGraphql\Connections\Provider\HospitalAffilliationToProvider;
use UHFPGraphql\Connections\Provider\ProfessionalProfileToProvider;
use UHFPGraphql\Connections\Provider\SpecialtyToProvider;
use UHFPGraphql\Connections\Provider\SubspecialtyToProvider;
use UHFPGraphql\Connections\Provider\TrainingToProvider;
use UHFPGraphql\Connections\Provider\WebsiteToProvider;

class ConnectionRegister {
  public function __construct()
  {
    array_filter(get_class_methods($this), function ($method) {
      if ($method !== '__construct') {
        add_action('graphql_register_types', [$this, $method], 99);
      }
    });
  }

  public function registerClinicConnection() {
    (new ClinicToRoot)->registerConnection();
  }

  public function registerEducationConnection() {
    (new EducationToProvider)->registerConnection();
  }

  public function registerHonorAndAwardsConnection() {
    (new HonorAndAwardsToProvider)->registerConnection();
  }

  public function registerSpecialtyProviderConnection()
  {
    (new SpecialtyToProvider)->registerConnection();
  }

  public function registerSubspecialtyProviderConnection()
  {
    (new SubspecialtyToProvider)->registerConnection();
  }

  public function registerBoardSpecialtiesProviderConnection()
  {
    (new BoardSpecialtiesToProvider)->registerConnection();
  }

  public function registerClinicsProviderConnection()
  {
    (new ClinicsToProvider)->registerConnection();
  }

  public function registerHospitalAffilliationProviderConnection()
  {
    (new HospitalAffilliationToProvider)->registerConnection();
  }

  // public function registerProfessionalProfileProviderConnections()
  // {
  //   (new ProfessionalProfileToProvider)->registerConnection();
  // }

  public function registerTrainingConnections() {
    (new TrainingToProvider)->registerMultipleConnections();
  }

  public function registerWebsiteConnection() {
    (new WebsiteToProvider)->registerConnection();
  }

  public function registerProviderConnection() {
    (new ProviderToRoot)->registerConnection();
  }

  public function registerSpecialtyConnection() {
    (new SpecialtyToRoot)->registerConnection();
  }
}