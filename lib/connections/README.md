The connection classes bring together the Types, Fields, and Resolvers as needed.

For instance to get a list of clinics, you need to connect
- the `Clinic` type to the `Root` type
- which exposes the `clinics` field
- which will resolve a list of clinics