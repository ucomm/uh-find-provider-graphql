<?php

namespace UHFPGraphql\Connections;

class CustomConnection implements ConnectionInterface {

  protected $connectionArgs;

  public function __construct()
  {

    // allow querying the CFAR test API
    $this->connectionArgs = [
      'devEndpoint' => [
        'type' => 'Bool',
        'description' => 'For testing only - Use the CFAR test API'
      ],
      'useCache' => [
        'type' => 'Bool',
        'description' => 'For testing only - toggle caching behavior'
      ]
    ];
  }

  public function registerConnection()
  {
    register_graphql_connection($this->getConfig());
  }

  public function registerMultipleConnections()
  {
    $connections = $this->getMultipleConfig();
    array_map(function($connection) {
      register_graphql_connection($connection);
    }, $connections);

  }

  /**
   * Set the config array for each connection.
   *
   * @return array
   */
  protected function getConfig(): array {
    return [];
  }

  protected function getMultipleConfig(): array {
    return [];
  }
}