<?php

namespace UHFPGraphql\Connections;

use UHFPGraphql\Resolvers\SpecialtyResolver;

class SpecialtyToRoot extends CustomConnection
{
  protected function getConfig(): array
  {

    $specialtyArgs = [
      'category' => [
        'type' => [
          'non_null' => 'ProviderCategoryEnum'
        ],
        'description' => 'Either doctor or dentist'
      ],
      'id' => [
        'type' => 'Int',
        'description' => 'Get specialties by an ID'
      ],
      'isVisible' => [
        'type' => 'Bool',
        'description' => 'Only get publicly visible specialties'
      ],
      'names' => [
        'type' => [
          'list_of' => 'String'
        ],
        'description' => 'Get specialties by name'
      ]
    ];

    $specialtyArgs = array_merge($this->connectionArgs, $specialtyArgs);

    return [
      'fromType' => 'RootQuery',
      'toType' => 'Specialty',
      'fromFieldName' => 'specialties',
      'connectionArgs' => $specialtyArgs,
      'resolve' => function ($root, $args, $context, $info) {
        return (new SpecialtyResolver)->multipleNodesResolver($root, $args, $context, $info);
      }
    ];
  }
}
